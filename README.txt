FIXED PATH ALIAS DRUPAL MODULE
==============================

CONTENTS OF THIS FILE
---------------------

 * Summary
 * Requirements
 * Installation
 * Configuration
 * Usage
 * Related modules
 * Contact


SUMMARY
-------

Path aliases are content. Content is not considered as part of the application,
so it is not saved and deployed as the code or the configuration components.

This module provides a way to set selected aliases as part of the configuration.
With it, such aliases will be present in any environment.


REQUIREMENTS
------------

Just Drupal core.


INSTALLATION
------------

Install as usual, see https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

The module does not require configuration. See the usage to know how to set
a path alias as fixed.


USAGE
-----

 * go to Configuration -> Search and metadata -> URL aliases
 * add or edit an alias
 * enable the "Fixed alias" option added by this module


RELATED MODULES
---------------

Recommended modules:

 * Default content
   (https://www.drupal.org/project/default_content)
   Exports any content entity within the website code. Any new installation will
   start with the exported content.

 * Fixed block content
   (https://www.drupal.org/project/fixed_block_content)
   Provides fixed custom blocks that are exported to configuration and created
   on-demand.


CONTACT
-------

Current maintainers:
* Manuel Adan (manuel.adan) - https://www.drupal.org/user/516420
